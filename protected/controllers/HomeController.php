<?php

class HomeController extends Controller
{

	public function actions()
	{
		return array(
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
		);
	}	

	public function actionDummy()
	{
		Dummy::createDummyProduct();
		echo '<META http-equiv="refresh" content="0;URL=http://localhost/dv-computers/home/dummy">';
	}

	// public function actionImageupdate()
	// {
	// 	$data = SettingDescription::model()->findAll();
	// 	foreach ($data as $key => $value) {
	// 		$value->value = str_replace('/graphiccontrol/.dev/images/', '/images/', $value->value);
	// 		$value->save(false);
	// 	}

	// 	$data = PrdProductDescription::model()->findAll();
	// 	foreach ($data as $key => $value) {
	// 		$value->desc = str_replace('/graphiccontrol/.dev/images/', '/images/', $value->desc);
	// 		$value->note = str_replace('/graphiccontrol/.dev/images/', '/images/', $value->note);
	// 		$value->subtitle = str_replace('/graphiccontrol/.dev/images/', '/images/', $value->subtitle);
	// 		$value->save(false);
	// 	}
	// 	echo "success"; die();
	// }

	public function actionIndex()
	{
		$criteria=new CDbCriteria;

		$criteria->with = array('description', 'category', 'categories');

		$criteria->order = 'date DESC';

		$criteria->addCondition('status = "1"');
		$criteria->addCondition('terlaris = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		// $criteria->addCondition('categoryView.language_id = :language_id');
		// $criteria->addCondition('categoryTitle.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;

		$criteria->limit = 5;

		$product = PrdProduct::model()->findAll($criteria);

		$criteria = new CDbCriteria;
		$criteria->with = array('description');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$criteria->limit = 10;
		$dataBrand = Brand::model()->findAll($criteria);

		$model = new ContactForm;
		$model->scenario = 'insert';

		$this->layout='//layouts/column1';
		$this->render('index', array(
			'product'=>$product,
			'dataBrand'=>$dataBrand,
			'model'=>$model,
		));
	}

	public function actionSolution()
	{
		$criteria = new CDbCriteria;
		$criteria->with = array('description');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$dataBrand = Brand::model()->findAll($criteria);

		$this->pageTitle = 'Solutions - '. $this->pageTitle;

		$this->render('solution', array(	
			'dataBrand'=>$dataBrand,
		));
	}

	public function actionProducts()
	{
		$criteria=new CDbCriteria;
		$criteria->with = array('description', 'categories');
		$criteria->order = 'date DESC';
		$criteria->addCondition('status = "1"');
		// $criteria->addCondition('terlaris = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;

		$category = null;
		if ($_GET['category']) {
			$criteria->addCondition('t.category_id = :category_id');
			$criteria->params[':category_id'] = $_GET['category'];

			$criteria2 = new CDbCriteria;
			$criteria2->with = array('description');
			$criteria2->addCondition('t.id = :id');
			$criteria2->params[':id'] = $_GET['category'];
			$criteria2->addCondition('description.language_id = :language_id');
			$criteria2->params[':language_id'] = $this->languageID;
			$category = PrdCategory::model()->find($criteria2);
		}
		if ($_GET['solution']) {
			$criteria->addCondition('categories.category_id = :categories');
			$criteria->params[':categories'] = $_GET['solution'];
		}
		
		if ($_GET['type'] == 'collection') {
			$criteria->addCondition('t.collection = :collection');
			$criteria->params[':collection'] = 1;
		}
		if ($_GET['type'] == 'sale') {
			$criteria->addCondition('t.sale = :sale');
			$criteria->params[':sale'] = 1;
		}

		if ($_GET['q']) {
			$criteria->addCondition('description.name LIKE :q OR description.desc LIKE :q OR t.tag LIKE :q');
			$criteria->params[':q'] = '%'.$_GET['q'].'%';
		}
		$pageSize = 12;
		$criteria->group = 't.id';
		$product = new CActiveDataProvider('PrdProduct', array(
			'criteria'=>$criteria,
		    'pagination'=>array(
		        'pageSize'=>$pageSize,
		    ),
		));

		$this->pageTitle = 'Products - '. $this->pageTitle;
		$this->render('product', array(	
			'product'=>$product,
			'category'=>$category,
		));
	}
	
	public function actionProductDetail($id)
	{
		$criteria=new CDbCriteria;
		$criteria->with = array('description', 'category');
		$criteria->addCondition('status = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$criteria->addCondition('t.id = :id');
		$criteria->params[':id'] = $id;
		$data = PrdProduct::model()->find($criteria);
		if($data===null)
			throw new CHttpException(404,'The requested page does not exist.');

		$criteria=new CDbCriteria;
		$criteria->with = array('description', 'category', 'alternateImage');
		$criteria->order = 'date DESC';
		$criteria->addCondition('status = "1"');
		// $criteria->addCondition('terlaris = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;

			$criteria->addCondition('t.category_id = :category_id');
			$criteria->params[':category_id'] = $data->category_id;

		$pageSize = 3;
		$criteria->group = 't.id';
		$criteria->order = 'RAND()';
		$criteria->limit = 4;
		$product = PrdProduct::model()->findAll($criteria);

		$criteria=new CDbCriteria;
		$criteria->with = array('description', 'category', 'alternateImage');
		$criteria->order = 'date DESC';
		$criteria->addCondition('status = "1"');
		// $criteria->addCondition('terlaris = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;

		$pageSize = 3;
		$criteria->group = 't.id';
		$criteria->order = 'RAND()';
		$product2 = PrdProduct::model()->findAll($criteria);

		$this->pageTitle = 'Product Detail - '. $this->pageTitle;
		// detail product stumble	
		$this->render('product_detail', array(	
			'data'=>$data,
			'product'=>$product,
			'product2'=>$product2,
		));	

	}

	// ------------------------------- GAK DI PAKAI ------------------------------------
	public function actionCategory()
	{
		$this->layout='//layouts/column2';

		$this->render('category', array(
		));
	}

	public function actionPCart()
	{
		$this->layout='//layouts/column3';

		$this->render('carts', array(
		));
	}

	public function actionPCart2()
	{
		$this->layout='//layouts/column3';

		$this->render('carts2', array(
		));
	}

	public function actionPsuccesscart()
	{
		$this->layout='//layouts/column3';

		$this->render('success_cart', array(
		));
	}

	public function actionCaraBelanja()
	{
		$this->layout='//layouts/column3';

		$this->render('cara_belanja', array(
		));
	}

	public function actionInfoPengiriman()
	{
		$this->layout='//layouts/column3';

		$this->render('info_pengiriman', array(
		));
	}

	public function actionSyarat()
	{
		$this->layout='//layouts/column3';

		$this->render('syarat_ketentuan', array(
		));
	}

	public function actionFaq()
	{
		$this->layout='//layouts/column3';

		$this->render('faq', array(
		));
	}

	public function actionPmyaccount()
	{
		$this->layout='//layouts/column3';

		$this->render('mydashboard', array(
		));
	}

	public function actionPcontact()
	{
		$model = new ContactForm;
		$model->scenario = 'insert';
		
		if(isset($_POST['ContactForm']))
		{
			if (!isset($_POST['g-recaptcha-response'])) {
				$this->redirect(array('home/career'));
	        }
	        

				$model->attributes=$_POST['ContactForm'];
				if($model->validate())
				{
			        $secret_key = "6Lc5ExQUAAAAAHgV4U_6krEDyf-ykhlx08mEgJek";
			        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
			        $response = json_decode($response);
			        if($response->success==false)
			        {
			          $model->addError('verifyCode','Pastikan anda sudah menyelesaikan captcha');
			        }else{
						// config email
						$messaged = $this->renderPartial('//mail/contact',array(
							'model'=>$model,
						),TRUE);
						$config = array(
							'to'=>array($model->email, $this->setting['email'], $this->setting['contact_email']),
							'subject'=>'[Victory Toys] Contact from '.$model->email,
							'message'=>$messaged,
						);
						if ($this->setting['contact_cc']) {
							$config['cc'] = array($this->setting['contact_cc']);
						}
						if ($this->setting['contact_bcc']) {
							$config['bcc'] = array($this->setting['contact_bcc']);
						}
						// kirim email
						Common::mail($config);

						Yii::app()->user->setFlash('success','Trimakasih telah mengirimkan pesan kepada kami, kami akan segera membalas pesan anda');
						$this->refresh();
					}
				}


		}

		$this->layout='//layouts/column3';
		$this->pageTitle = 'Contact Us - '.$this->pageTitle;
		$this->render('pcontact', array(
			'model'	=>$model,
		));
	}

	public function actionPsaler()
	{
		
		$model = new ContactForm;
		$model->scenario = 'insert';
		
		if(isset($_POST['ContactForm']))
		{
			if (!isset($_POST['g-recaptcha-response'])) {
				$this->redirect(array('home/career'));
	        }
	        

				$model->attributes=$_POST['ContactForm'];
				if($model->validate())
				{
			        $secret_key = "6Lfhyf8SAAAAABJ2p1sV8mV790VW7LAVOsy2qile";
			        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
			        $response = json_decode($response);
			        // print_r($response);
			        // exit;
			        if($response->success==false)
			        {
			          $model->addError('verifyCode','Pastikan anda sudah menyelesaikan captcha');
			        }else{
						// config email
						$messaged = $this->renderPartial('//mail/saler',array(
							'model'=>$model,
						),TRUE);
						$config = array(
							'to'=>array($model->email, $this->setting['email'], $this->setting['contact_email']),
							'subject'=>'Hi, Ada pendaftaran wholesaler '.$model->email,
							'message'=>$messaged,
						);
						if ($this->setting['contact_cc']) {
							$config['cc'] = array($this->setting['contact_cc']);
						}
						if ($this->setting['contact_bcc']) {
							$config['bcc'] = array($this->setting['contact_bcc']);
						}
						// kirim email
						Common::mail($config);

						Yii::app()->user->setFlash('success','Trimakasih telah mengirimkan pesan kepada kami, kami akan segera membalas pesan anda');
						$this->refresh();
					}

				}

		}
		$this->layout='//layouts/column1';
		$this->render('psaler', array(
			'model'	=>$model,
		));
	}

	public function actionSugest()
	{

		if ($_POST['q'] != '') {
			$str = '<p id="searchresults">';
	            $criteria=New CDbCriteria ; 
	            $criteria->addCondition('(name LIKE :q OR tag LIKE :q)');
	            $criteria->params[':q'] = '%'.$_POST['q'].'%';
	            $criteria->addCondition('language_id = :language_id');
	            $criteria->params[':language_id'] = $this->languageID;

	            $criteria->order = 'date_input DESC';
	            $criteria->limit = 5;
	            $list = ViewProduct::model()->findAll($criteria);
	            
				$str .= '<span class="category">Search: '.$_POST['q'].'</span>';
	            foreach($list as $value)
	            {
					$str .= '<a href="'.CHtml::normalizeUrl(array('/product/detail', 'id'=>$value->id)).'">
						<span class="searchheading">'.$value->name.'</span>
					</a>';
				}
			// }
			$str .= '<span class="seperator"><a href="'.CHtml::normalizeUrl(array('/product/index', 'q'=>$_POST['q'])).'" title="Sitemap">See other result for '.$_POST['q'].' &gt;</a></span>
			<br class="break">
			</p>
			';
			echo $str;
		}
	}

	public function actionError()
	{
		$this->layout = '//layouts/error';
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else{

				$criteria=new CDbCriteria;
				$criteria->with = array('description', 'category', 'categories');
				$criteria->order = 'date DESC';
				$criteria->addCondition('status = "1"');
				$criteria->addCondition('terlaris = "1"');
				$criteria->addCondition('description.language_id = :language_id');
				$criteria->params[':language_id'] = $this->languageID;
				$pageSize = 12;
				$criteria->group = 't.id';
				$product = new CActiveDataProvider('PrdProduct', array(
					'criteria'=>$criteria,
				    'pagination'=>array(
				        'pageSize'=>$pageSize,
				    ),
				));


				$criteria = new CDbCriteria;
				$criteria->with = array('description');
				$criteria->addCondition('t.parent_id = :parent_id');
				$criteria->params[':parent_id'] = 0;
				$criteria->addCondition('t.type = :type');
				$criteria->params[':type'] = 'category';
				$criteria->limit = 3;
				$criteria->order = 'sort ASC';
				$categories = PrdCategory::model()->findAll($criteria);

				$this->layout='//layouts/column2';

				$this->pageTitle = 'Error '.$error['code'].': '. $error['message'] .' - '.$this->pageTitle;
				$this->render('error', array(
					'error'=>$error,
					'product'=>$product,
					'categories'=>$categories,
				));
			}
		}

	}

	public function actionAbout()
	{
		$this->pageTitle = 'About Us - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('about', array(	
		));
	}

	public function actionArtikel()
	{
		$this->pageTitle = 'Artikel - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('artikel', array(	
		));
	}

	public function actionArtikeldet()
	{
		$this->pageTitle = 'Artikel - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('artikel_detail', array(	
		));
	}

	public function actionGallery()
	{
		$this->layout='//layouts/column2';
		$this->pageTitle = 'Gallery - '.$this->pageTitle;

		$criteria = new CDbCriteria;
		$criteria->addCondition('active = "1"');
		$criteria->addCondition('language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$criteria->order = 'date_input DESC';

		$model = ViewGallery::model()->findAll($criteria);

		$this->render('gallery', array(	
			'model'=> $model,
		));
	}

	public function actionWarranty()
	{
		$this->pageTitle = 'Warranty & Quality - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('warranty', array(	
		));
	}

	public function actionEvent()
	{
		$this->pageTitle = 'Events - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$model = new ContactForm;
		$model->scenario = 'insert';

		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];

			if($model->validate())
			{
				// config email
				$messaged = $this->renderPartial('//mail/contact',array(
					'model'=>$model,
				),TRUE);
				$config = array(
					'to'=>array($model->email, $this->setting['email'], $this->setting['contact_email']),
					'subject'=>'[Stumble Upon] Contact from '.$model->email,
					'message'=>$messaged,
				);
				if ($this->setting['contact_cc']) {
					$config['cc'] = array($this->setting['contact_cc']);
				}
				if ($this->setting['contact_bcc']) {
					$config['bcc'] = array($this->setting['contact_bcc']);
				}
				// kirim email
				Common::mail($config);

				Yii::app()->user->setFlash('success','Thank you for contact us. We will respond to you as soon as possible.');
				$this->refresh();
			}

		}

		$criteria = new CDbCriteria;
		$criteria->with = array('description');
		$criteria->addCondition('active = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$criteria->order = 'date_input DESC';
		$gallery = new CActiveDataProvider('Gallery', array(
			'criteria'=>$criteria,
		    'pagination'=>array(
		        'pageSize'=>6,
		    ),
		));

		$this->render('event', array(
			'model'=>$model,
			'gallery'=>$gallery,
		));
	}

	public function actionEventdetail($id)
	{
		$this->pageTitle = 'Events - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$criteria = new CDbCriteria;
		$criteria->with = array('description');
		$criteria->addCondition('active = "1"');
		$criteria->addCondition('description.language_id = :language_id');
		$criteria->params[':language_id'] = $this->languageID;
		$criteria->addCondition('t.id = :id');
		$criteria->params[':id'] = $id;
		$criteria->order = 'date_input DESC';
		$detail = Gallery::model()->find($criteria);

		$this->render('eventdetail', array(
			'detail'=>$detail,
		));
	}

	public function actionWhereto()
	{
		$this->pageTitle = 'Where To Buy - '. $this->pageTitle;

		$criteria = new CDbCriteria;
		if ($_GET['kota'] != '') {
			$criteria->addCondition('kota = :kota');
			$criteria->params[':kota'] = $_GET['kota'];
		}
		if ($_GET['provinsi'] != '') {
			$criteria->addCondition('provinsi = :provinsi');
			$criteria->params[':provinsi'] = $_GET['provinsi'];
			$dataAddres = Address::model()->findAll($criteria);
		}

		$criteria = new CDbCriteria;
		$criteria->select = 'kota';
		$criteria->group = 'kota';
		$criteria->order = 'kota ASC';
		$kota = Address::model()->findAll($criteria);

		$criteria = new CDbCriteria;
		$criteria->select = 'provinsi';
		$criteria->group = 'provinsi';
		$criteria->order = 'provinsi ASC';
		$provinsi = Address::model()->findAll($criteria);

		$this->render('where_to', array(
			'dataAddres'=>$dataAddres,
			'kota'=>$kota,
			'provinsi'=>$provinsi,
		));
	}

	public function actionGetkota()
	{
		$criteria = new CDbCriteria;
		$criteria->select = 'kota';
		$criteria->group = 'kota';
		$criteria->order = 'kota ASC';
		if ($_POST['provinsi'] != '') {
			$criteria->addCondition('provinsi = :provinsi');
			$criteria->params[':provinsi'] = $_POST['provinsi'];
		}
		$kota = Address::model()->findAll($criteria);
		foreach ($kota as $key => $value) {
			?>
			<option value="<?php echo $value->kota ?>"><?php echo $value->kota ?></option>
			<?php
		}
	}

	public function actionHowto()
	{
		$this->pageTitle = 'How To Order - '.$this->pageTitle;
		$this->layout='//layouts/column2';

		$this->render('howto', array(	
		));
	}

	public function actionBlogs()
	{
		$this->pageTitle = 'Berita & Artikel - '.$this->pageTitle;
		$this->layout='//layouts/column1';

		$this->render('blogs', array(	
		));
	}

	public function actionBlogDetail()
	{
		$this->pageTitle = 'Berita & Artikel - '.$this->pageTitle;
		$this->layout='//layouts/column1';

		$this->render('blog_detail', array(	
		));
	}

	public function actionLandingproduct()
	{
		$this->pageTitle = 'Products - '. $this->pageTitle;

		$this->render('landing_product', array(	
		));
	}
	
	public function actionProductCategory()
	{
		$this->pageTitle = 'Products - '. $this->pageTitle;

		$this->render('product_category', array(	
		));
	}

	public function actionProducts2()
	{
		$this->pageTitle = 'Products - '. $this->pageTitle;

		$this->render('product2', array(	
		));
	}

	public function actionListInquiry()
	{
		$this->pageTitle = 'Daftar Inquiry - '. $this->pageTitle;
		$this->layout='//layouts/column1';

		$this->render('inquiry', array(	
		));
	}

	public function actionDetailInquiry()
	{
		$this->pageTitle = 'Daftar Inquiry - '. $this->pageTitle;
		$this->layout='//layouts/column1';

		$this->render('detail_inquiry', array(	
		));
	}

	public function actionJadiagen()
	{
		$this->pageTitle = 'Jadi Agen - '.$this->pageTitle;
		$this->layout='//layouts/column1';

		$model = new ContactForm;
		$model->scenario = 'insert';

		if(isset($_POST['ContactForm']))
		{
			// if (!isset($_POST['g-recaptcha-response'])) {
			// 	$this->redirect(array('/home/jadiagen'));
	  //       }
	        
	        $secret_key = "6LfaqgkUAAAAAEGhdM7GVk6o-jNbidJ9t3xgc0wn";
	        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
	        $response = json_decode($response);
	        if($response->success==false)
	        {
	          $model->addError('verifyCode', 'Silahkan verifikasi captcha yang tersedia');
	        }

			$model->attributes=$_POST['ContactForm'];
			if(!$model->hasErrors() AND $model->validate())
			{
				// config email
				$messaged = $this->renderPartial('//mail/contact',array(
					'model'=>$model,
				),TRUE);
				$config = array(
					'to'=>array($model->email, $this->setting['email']),
					'subject'=>'Hi, '.$model->email.' Ingin Jadi Agen',
					'message'=>$messaged,
				);
				if ($this->setting['contact_cc']) {
					$config['cc'] = array($this->setting['contact_cc']);
				}
				if ($this->setting['contact_bcc']) {
					$config['bcc'] = array($this->setting['contact_bcc']);
				}
				// kirim email
				Common::mail($config);

				Yii::app()->user->setFlash('success','Terima Kasih Atas Minat Anda Bergabung Bersama kami
Staff dari perabotplastik.com akan menghubungi anda untuk konfirmasi dan penjelasan berbagai langkah selanjutnya untuk secara resmi menjadi agen distribusi produk kami.
');
				$this->refresh();
			}


		}

		$this->render('jadi_agen', array(
			'model' => $model,
		));
	}

	public function actionKatalog()
	{
		$this->pageTitle = 'Katalog - '. $this->pageTitle;

		$this->layout='//layouts/column1';

		$dataPdf = Pdf::model()->findAll();

		$this->render('katalog', array(	
			'dataPdf'=>$dataPdf,
		));
	}

	public function actionLokasitoko()
	{
		$this->pageTitle = 'Lokasi Penjualan Kami - '. $this->pageTitle;

		$this->layout='//layouts/column1';
		$dataAddress = Address::model()->findAll();
		$dataAddress2 = array();
		foreach ($dataAddress as $key => $value) {
			$dataAddress2[$value->kota][] = $value;
		}

		$this->render('lokasi_toko', array(	
			'dataAddress'=>$dataAddress2,
		));
	}

	public function actionCarabeli()
	{
		$this->pageTitle = 'Cara Membeli - '. $this->pageTitle;

		$this->layout='//layouts/column1';

		$this->render('cara_beli', array(	
		));
	}

	public function actionContactus()
	{
		$this->pageTitle = 'Contact - '.$this->pageTitle;
		$this->layout='//layouts/column2';
		
		$model = new ContactForm;
		$model->scenario = 'insert';

		if(isset($_POST['ContactForm']))
		{
			if (!isset($_POST['g-recaptcha-response'])) {
				$this->redirect(array('/home/contactus'));
	        }
	        
	        $secret_key = "6LfaqgkUAAAAAEGhdM7GVk6o-jNbidJ9t3xgc0wn";
	        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
	        $response = json_decode($response);
	        if($response->success==false)
	        {
	          $model->addError('verifyCode', 'Silahkan verifikasi captcha yang tersedia');
	        }

			$model->attributes=$_POST['ContactForm'];
			if(!$model->hasErrors() AND $model->validate())
			{
				// config email
				$messaged = $this->renderPartial('//mail/contact',array(
					'model'=>$model,
				),TRUE);
				$config = array(
					'to'=>array($model->email, $this->setting['email']),
					'subject'=>'Hi, Contact from '.$model->email,
					'message'=>$messaged,
				);
				if ($this->setting['contact_cc']) {
					$config['cc'] = array($this->setting['contact_cc']);
				}
				if ($this->setting['contact_bcc']) {
					$config['bcc'] = array($this->setting['contact_bcc']);
				}
				// kirim email
				Common::mail($config);

				Yii::app()->user->setFlash('success','Thank you for sending application career us. We will respond to you as soon as possible.');
				$this->refresh();
			}

		}

		$this->render('contact', array(	
			'model'=>$model,
		));
	}

	// public function actionCareer()
	// {
	// 	$this->pageTitle = 'Career - '.$this->pageTitle;
	// 	$this->layout='//layouts/column2';

	// 	$model = new ContactForm;
	// 	$model->scenario = 'insert';

	// 	if(isset($_POST['ContactForm']))
	// 	{
	// 		if (!isset($_POST['g-recaptcha-response'])) {
	// 			$this->redirect(array('home/career'));
	//         }
	        
	//         $secret_key = "6Lfhyf8SAAAAABJ2p1sV8mV790VW7LAVOsy2qile";
	//         $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
	//         $response = json_decode($response);
	//         if($response->success==false)
	//         {
	//           $this->redirect(array('home/career'));
	//         }else{

	// 			$model->attributes=$_POST['ContactForm'];
	// 			if($model->validate())
	// 			{
	// 				// config email
	// 				$messaged = $this->renderPartial('//mail/contact',array(
	// 					'model'=>$model,
	// 				),TRUE);
	// 				$config = array(
	// 					'to'=>array($model->email, $this->setting['email'], $this->setting['contact_email']),
	// 					'subject'=>'Hi, Contact from '.$model->email,
	// 					'message'=>$messaged,
	// 				);
	// 				if ($this->setting['contact_cc']) {
	// 					$config['cc'] = array($this->setting['contact_cc']);
	// 				}
	// 				if ($this->setting['contact_bcc']) {
	// 					$config['bcc'] = array($this->setting['contact_bcc']);
	// 				}
	// 				// kirim email
	// 				Common::mail($config);

	// 				Yii::app()->user->setFlash('success','Thank you for sending application career us. We will respond to you as soon as possible.');
	// 				$this->refresh();
	// 			}

	// 		}

	// 	}

	// 	$this->render('career', array(
	// 		'model'	=>$model,
	// 	));
	// }

	public function actionNews()
	{
		$this->pageTitle = 'News & Article - '.$this->pageTitle;
		$this->layout='//layouts/column1';

		$this->render('news', array(	
		));
	}

	public function actionNewsDetail()
	{
		$this->pageTitle = 'News & Article - '.$this->pageTitle;
		$this->layout='//layouts/column1';

		$this->render('news_detail', array(	
		));
	}

	public function actionTermsofuse()
	{
		$this->pageTitle = 'Terms of Use - '.$this->pageTitle;

		$this->render('termsofuse', array(	
		));
	}

	public function actionShippinginfo()
	{
		$this->render('shipinfo', array(	
		));
	}

	public function actionCartsuccess()
	{
		$this->render('cartsucess', array(	
		));
	}

	public function actionCareer()
	{
		$this->layout='//layouts/column2';

		$this->pageTitle = 'Career - '.$this->pageTitle;

		$this->render('career', array(
			// 'model'=>$model,
		));
	}

	public function actionContact()
	{
		$this->layout='//layouts/column2';

		$this->pageTitle = 'Contact Us - '.$this->pageTitle;

		$model = new ContactForm;
		$model->scenario = 'insert';

		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];

	        $secret_key = "6Lc6BFkUAAAAAN5bcwg9UnOPMipwIzSDkaPIXOEA";
	        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$_POST['g-recaptcha-response']."&remoteip=".$_SERVER['REMOTE_ADDR']);
	        $response = json_decode($response);
	        if($response->success==false)
	        {
	          	$model->addError('verifyCode','Pastikan anda sudah menyelesaikan captcha');
	        }else{

				if(!$model->hasErrors() && $model->validate())
				{
					// config email
					$messaged = $this->renderPartial('//mail/contact',array(
						'model'=>$model,
					),TRUE);
					$config = array(
						'to'=>array($model->email, $this->setting['email']),
						'subject'=>'['.Yii::app()->name.'] Contact from '.$model->email,
						'message'=>$messaged,
					);
					if ($this->setting['contact_cc']) {
						$config['cc'] = array($this->setting['contact_cc']);
					}
					if ($this->setting['contact_bcc']) {
						$config['bcc'] = array($this->setting['contact_bcc']);
					}
					// kirim email
					Common::mail($config);

					Yii::app()->user->setFlash('success','Thank you for contact us. We will respond to you as soon as possible.');
					$this->refresh();
				}
			}

		}

		$this->render('contact', array(
			'model'=>$model,
		));
	}

	public function actionContact2()
	{
		$this->layout='//layouts/columnIframe';

		$this->pageTitle = 'Contact Us - '.$this->pageTitle;

		$model = new ContactForm;
		$model->scenario = 'insert';

		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];

			if($model->validate())
			{
				// config email
				$messaged = $this->renderPartial('//mail/contact2',array(
					'model'=>$model,
				),TRUE);
				$config = array(
					'to'=>array($model->email, $this->setting['email'], $this->setting['contact_email'], 'renndh2003@hotmail.com', 'dvcomputers.website@outlook.com'),
					'subject'=>'Hi, DV Computers Contact from '.$model->email,
					'message'=>$messaged,
				);
				if ($this->setting['contact_cc']) {
					$config['cc'] = array($this->setting['contact_cc']);
				}
				if ($this->setting['contact_bcc']) {
					$config['bcc'] = array($this->setting['contact_bcc']);
				}
				// kirim email
				Common::mail($config);

				Yii::app()->user->setFlash('success','Thank you for contact us. We will respond to you as soon as possible.');
				$this->refresh();
			}

		}

		$this->render('contact2', array(
			'model'=>$model,
		));
	}
	public function actionContact3()
	{
		$this->layout='//layouts/columnIframe';

		$this->pageTitle = 'Report Bugs & Error - '.$this->pageTitle;

		$model = new ContactForm2;
		$model->scenario = 'insert';

		if(isset($_POST['ContactForm2']))
		{
			$model->attributes=$_POST['ContactForm2'];

			if($model->validate())
			{
				// config email
				$messaged = $this->renderPartial('//mail/contact3',array(
					'model'=>$model,
				),TRUE);
				$config = array(
					'to'=>array($model->email, $this->setting['email'], $this->setting['contact_email'], 'renndh2003@hotmail.com', 'dvcomputers.website@outlook.com'),
					'subject'=>'Report Bugs & Error from '.$model->email,
					'message'=>$messaged,
				);
				if ($this->setting['contact_cc']) {
					$config['cc'] = array($this->setting['contact_cc']);
				}
				if ($this->setting['contact_bcc']) {
					$config['bcc'] = array($this->setting['contact_bcc']);
				}
				// kirim email
				Common::mail($config);

				Yii::app()->user->setFlash('success','Thank you for contact us. We will respond to you as soon as possible.');
				$this->refresh();
			}

		}

		$this->render('contact3', array(
			'model'=>$model,
		));
	}

}

