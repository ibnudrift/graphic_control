<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>

<?php echo $this->renderPartial('//layouts/_header', array()); ?>

<?php /*
<!-- Start fcs -->
<div class="fcs-wrapper outers_fcs_wrapper prelatife">
    <div id="myCarousel_home" class="carousel fade" data-ride="carousel">
        <?php
        $criteria=new CDbCriteria;
        $criteria->with = array('description');
        $criteria->addCondition('description.language_id = :language_id');
        $criteria->params[':language_id'] = $this->languageID;
        $criteria->group = 't.id';
        $criteria->order = 't.id ASC';
        $slide = Slide::model()->with(array('description'))->findAll($criteria);
        ?>
        <div class="blocks_n_pag">
            <div class="prelatife container">
              <ol class="carousel-indicators">
            <?php foreach ($slide as $key => $value): ?>
                <li data-target="#myCarousel_home" data-slide-to="<?php echo $key ?>" class="<?php if ($key == 0): ?>active<?php endif ?>"></li>
            <?php endforeach ?>
              </ol>
            </div>
        </div>
        <div class="carousel-inner">
            <?php foreach ($slide as $key => $value): ?>
            <div class="item <?php if ($key == 0): ?>active<?php endif ?>">
                <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1586,581, '/images/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->description->title ?>" class="img-responsive hidden-xs">
                <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(774,867, '/images/'.$value->image2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="<?php echo $value->description->title ?>" class="img-responsive visible-xs">
                
                <div class="carousel-caption">
                    <div class="bxsl_tx_fcs">

                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <?php endforeach ?>
        </div>
        <div class="blocks_in_tfcs">
            <div class="prelatife container">
                <div class="ins_tFcs">
                    <h4><?php echo $this->setting['slide_title'] ?></h4>
                    <p><?php echo $this->setting['slide_content'] ?></p>
                    <div class="clear height-20"></div>
                    <div class="pl110">
                        <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(391,222, '/images/static/'.$this->setting['slide_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive" style="margin-left: -30px;">
                        <a href="<?php echo $this->setting['slide_url'] ?>"><i class="fa fa-chevron-right"></i> &nbsp;See our full range of products</a>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
</div>
<!-- End fcs -->
*/ ?>

<?php echo $content; ?>

<?php echo $this->renderPartial('//layouts/_footer', array()); ?>
<?php $this->endContent(); ?>