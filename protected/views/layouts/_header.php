<?php 
    $e_activemenu = $this->action->id;
    $controllers_ac = $this->id;
    $session=new CHttpSession;
    $session->open();
    $login_member = $session['login_member'];

    $active_menu_pg = $controllers_ac.'/'.$e_activemenu;
?>
<header class="header">
    <nav class="navbar visible-md visible-lg">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <a class="navbar-brand" href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
                        <img src="<?php echo $this->assetBaseurl; ?>logo1.png" data-src-res="<?php echo $this->assetBaseurl; ?>graphic_control.png" data-src-res2="<?php echo $this->assetBaseurl; ?>logo1.png">
                    </a>
                </div>
                  <div class="col-md-9">
                      <div class="menu">
                          <div class="login" style="padding-top: 10px;">
                              <div class="text-right" style="display:none; visibility: hidden;">
                                  <a href="#">Log in</a>
                                  <a href="#">Register</a>
                              </div>
                              <div class="text-right right">
                                <a style="padding:0px; display: block;" href="https://wa.me/6285706168917?text=Saya%20ingin%20informasi%20produk%20Graphic%20Control"><img src="<?php echo $this->assetBaseurl; ?>wa-chats-deks.png" alt="" class="img-responsive"></a>
                              </div>
                              <div class="clear clearfix"></div>
                          </div>     
                          <div class="text-right menu-list">
                              <div class="collapse navbar-collapse">
                                  <ul class="nav navbar-nav navbar-right">
                                      <a class="list" href="<?php echo CHtml::normalizeUrl(array('/home/solution')); ?>">Solutions</a>
                                      <a class="list" href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">About Us</a>
                                      <a class="list" href="<?php echo CHtml::normalizeUrl(array('/home/warranty')); ?>">Warranty & Quality</a>
                                      <a class="list" href="<?php echo CHtml::normalizeUrl(array('/home/career')); ?>">Career</a>
                                      <a class="list" href="<?php echo CHtml::normalizeUrl(array('/blog/index')); ?>">Article</a>
                                      <a class="list" href="<?php echo CHtml::normalizeUrl(array('/home/gallery')); ?>">Gallery</a>
                                      <a class="list" href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">Support & Contact</a>
                                      <a class="list morph" href="https://wa.me/6285706168917?text=Saya%20ingin%20informasi%20produk%20Graphic%20Control"><img src="<?php echo $this->assetBaseurl; ?>wa-chats-deks.png" alt="" class="img-responsive"></a>
                                  </ul>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </nav>
    <nav class="navbar navbar-bot visible-md visible-lg">
        <div class="container">
            <div class="menu">
              <div class="row">
                <div class="col-md-11">
                  <div class="collapse navbar-collapse">
                      <?php
                          $criteria = new CDbCriteria;
                          $criteria->with = array('description');
                          $criteria->addCondition('description.language_id = :language_id');
                          $criteria->params[':language_id'] = $this->languageID;
                          $criteria->addCondition('t.type = :type');
                          $criteria->params[':type'] = 'category';
                          $criteria->addCondition('t.parent_id = :parent_id');
                          $criteria->params[':parent_id'] = 0;
                          $criteria->order = 'sort ASC';
                          $criteria->limit = 8;
                          $categories = PrdCategory::model()->findAll($criteria);
                      ?>
                      <ul class="nav navbar-nav navbar-left">
                          <li class="first"><a class="list-2" href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Our Products</a></li>
                          <?php foreach ($categories as $key => $value): ?>
                          <li <?php if ($_GET['category'] == $value->id OR $_GET['parent'] == $value->id): ?>class="active"<?php endif ?>><a class="list-2" href="<?php echo CHtml::normalizeUrl(array('/home/products', 'category'=>$value->id)); ?>"><?php echo $value->description->name ?></a>
                            <?php
                            $criteria = new CDbCriteria;
                            $criteria->with = array('description');
                            $criteria->addCondition('description.language_id = :language_id');
                            $criteria->params[':language_id'] = $this->languageID;
                            $criteria->addCondition('t.type = :type');
                            $criteria->params[':type'] = 'category';
                            $criteria->addCondition('t.parent_id = :parent_id');
                            $criteria->params[':parent_id'] = $value->id;
                            $criteria->order = 'sort ASC';
                            $sub_categories = PrdCategory::model()->findAll($criteria);
                            ?>
                            <?php if (count($sub_categories) > 0): ?>
                            <ul class="dropdown-menu">
                              <?php foreach ($sub_categories as $ke => $valu) { ?>
                                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/products', 'category'=>$valu->id, 'parent'=> $value->id)); ?>"><?php echo $valu->description->name ?></a></li>
                              <?php } ?>
                            </ul>
                            <?php endif ?>
                          </li>
                          <?php endforeach ?>
                          <li class="feature blink"><a href="<?php echo CHtml::normalizeUrl(array('/home/products', 'type'=>'sale')); ?>">Clearance Sale</a></li>
                          <li class="feature blink"><a href="<?php echo CHtml::normalizeUrl(array('/home/products', 'type'=>'collection')); ?>">Collection</a></li>
                      </ul> 
                  </div>
                </div>
                <div class="col-md-1">
                  <div class="text-right rights_hed_search">
                    <button class="viewsn_open btn btn-link p-0">
                      <img src="<?php echo $this->assetBaseurl ?>icon-searchs.png" alt="" class="img img-fluid">
                    </button>
                    <div class="posans_searchs_fm">
                      <form class="form-inline" method="get" action="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">
                        <div class="form-group mb-2">
                          <input type="text" class="form-control" id="inputPassword2" name="q" placeholder="Item code, nama produk...">
                        </div>
                        <button type="submit" class="btn btn-primary mb-2">Cari</button>
                      </form>
                      <div class="clear"></div>
                    </div>
                    <div class="clear clearfix"></div>
                  </div>
                  <?php /*<div id="wrap_search" class="box-search-formrights">
                    <form id="custom-search-form" class="form-search form-horizontal pull-right" action="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>" autocomplete="on">
                    <input id="search" name="q" type="text" placeholder="Search"><button id="search_submit" type="button"></button>
                    </form>
                  </div>
                  */ ?>

                  <script type="text/javascript">
                    $(document).ready(function(){

                      $( ".rights_hed_search button.viewsn_open" ).live('click', function(){
                        $(this).addClass('opened');
                        $('.rights_hed_search .posans_searchs_fm').fadeIn();                        
                      });

                      $( ".rights_hed_search button.viewsn_open.opened" ).live('click', function(){
                        $(this).removeClass('opened');
                        $('.rights_hed_search .posans_searchs_fm').fadeOut();                        
                      });

                    });
                  </script>

                </div>
              </div>

              <div class="clear"></div>
            </div>
        </div>
    </nav>
    
    <div class="visible-sm visible-xs">
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">
              <img src="<?php echo $this->assetBaseurl ?>graphic_control.png" alt="" class="img-responsive">
            </a>
          </div>

          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Our Products</a>
                <ul class="dropdown-menu">
                  <?php foreach ($categories as $key => $value): ?>
                  <li <?php if ($_GET['category'] == $value->id): ?>class="active"<?php endif ?>><a class="list-2" href="<?php echo CHtml::normalizeUrl(array('/home/products', 'category'=>$value->id)); ?>"><?php echo $value->description->name ?></a></li>
                  <?php endforeach ?>
                </ul>
              </li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/solution')); ?>">Solutions</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">About Us</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/warranty')); ?>">Warranty & Quality</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/career')); ?>">Career</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/blog/index')); ?>">Article</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/gallery')); ?>">Gallery</a></li>
              <li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">Support & Contact</a></li>
            </ul>

          </div>
        </div>
      </nav>
      <div class="clear"></div>
    </div>

  <div class="clear"></div>
</header>

<script type="text/javascript">
  $(document).ready(function(){
      // $('.nl_popup a').live('hover', function(){
      //     $('.popup_carts_header').fadeIn();
      // });
      // $('.popup_carts_header').live('mouseleave', function(){
      //   setTimeout(function(){ 
      //       $('.popup_carts_header').fadeOut();
      //   }, 500);
      // });
  });
</script>


<div class="blocks_mob_bottoms_wa visible-xs">
  <div class="prelatife container text-center">
    <div class="row">
      <div class="col-md-12">
        <div class="tsx">
          <a href="https://wa.me/6285706168917?text=Saya%20ingin%20informasi%20produk%20Graphic%20Control"><img src="<?php echo $this->assetBaseurl ?>wa-chats-tmob.png" class="img-responsive" alt=""> &nbsp;Click Here To Start Whatsapp Chat</a>
        </div>
      </div>
    </div>
  </div>
</div>
<style type="text/css">
  .blocks_mob_bottoms_wa{
    position: fixed;
    bottom: 0px; left: 0px; z-index: 250;
    height: auto; width: 100%;
    background-color: #000;
    padding: 1.5rem 0;
  }
  .blocks_mob_bottoms_wa .tsx{
    font-size: 16px; font-weight: 700; color: #fff;
    clear: both;
  }
  .blocks_mob_bottoms_wa .tsx a{
    font-size: 16px; font-weight: 700; color: #fff; text-decoration: none;
  }
  .blocks_mob_bottoms_wa .tsx img{
    max-width: 28px;
    display: inline-block;
  }

  .morph .menu-list ul a.list.morph{
    display: inline-block;
    vertical-align: middle;
    padding-left: 20px
  }
  .morph .menu-list ul a.list.morph img{
    margin-top: 0;
  }
  .menu-list ul a.list.morph{
    display: none;
  }
  .header .navbar-bot .menu ul li a{
    padding: 8px 6px;
  }
</style>