<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<section class="headers_inside">
	<?php echo $this->renderPartial('//layouts/_header', array()); ?>
</section>

<?php echo $content ?>

<?php echo $this->renderPartial('//layouts/_footer', array()); ?>
<?php $this->endContent(); ?>