
<!-- pelayanan -->
<section class="pelayanan text-center">
    <div class="container">
        <a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><img class="logo" src="<?php echo $this->assetBaseurl; ?>logo-bot.png"></a>
        <div class="title">
            <p><?php echo $this->setting['contact_footer_title'] ?></p>
        </div>
        <div class="subtitle">
            <?php echo $this->setting['contact_footer_content'] ?>
        </div>

        <div class="row bottoms_footr">
            <div class="col-md-4 col-sm-4">
                <img src="<?php echo $this->assetBaseurl; ?>icon-phone.png"> 
                <p class="title">Hotline</p>
                <p><?php echo nl2br($this->setting['contact_hotline']) ?></p>             
            </div>
            <div class="col-md-4 col-sm-4">
                <img src="<?php echo $this->assetBaseurl; ?>icon-email.png"> 
                <p class="title">Email</p>
                <p><?php echo $this->setting['email'] ?><br>
                <a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>#contact-form">Click here for online inquiry</a></p>
            </div>
            <div class="col-md-4 col-sm-4">
                <img src="<?php echo $this->assetBaseurl; ?>icon-map.png"> 
                <p class="title">Head Office</p>
                <p><?php echo nl2br($this->setting['contact_headquarter_address']) ?><br>
                <a href="<?php echo $this->setting['contact_headquarter_map_url'] ?>">View on google map</a></p>
            </div>
        </div>
    </div>
</section>
<!-- akhir pelayanan -->

<!-- footer -->
<footer class="footer text-center">
    <p>Copyright &copy 2018 - PT. GRAPHIC CONTROL INDONESIA - Surabaya, Indonesia. Site by Mark Design.</p>    
</footer>
<!-- akhir footer -->

<?php
/*
<footer class="foot">
	<div class="prelatife container"> 
		<div class="clear height-45"></div>
		<div class="insides_footer">
			<div class="top_footer">
				<div class="row">
					<div class="col-md-4">
						<div class="lgo_footers"><a href="#"><img src="<?php echo $this->assetBaseurl; ?>lgo_footer.png" alt="" class="img-responsive"></a></div>
					</div>
					<div class="col-md-6">
						<div class="menu_footer">
							<ul class="list-inline">
				                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
				                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>">About Us</a></li>
				                <li><a href="<?php echo CHtml::normalizeUrl(array('/product/list')); ?>">Products</a></li>
				                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/brands')); ?>">Brands</a></li>
				                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/agent')); ?>">Become An Agent</a></li>
				                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/whereto')); ?>">Where to Buy</a></li>
				                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/career')); ?>">Career</a></li>
				                <li><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>">Contact Us</a></li>
							</ul>
						</div>
					</div>
					<div class="col-md-2">
						<div class="social_footer text-right">
							Connect with us &nbsp;&nbsp;&nbsp;
							<?php if ($this->setting['url_instagram'] != ''): ?>
							<a href="<?php echo $this->setting['url_instagram'] ?>"><i class="fa fa-instagram"></i></a>&nbsp;&nbsp;
							<?php endif ?>
							<?php if ($this->setting['url_facebook'] != ''): ?>
							<a href="<?php echo $this->setting['url_facebook'] ?>"><i class="fa fa-facebook"></i></a>
							<?php endif ?>
							<div class="clear"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="infos_bottom_footer">
						<h5>AUTOMOTIVE / CAR CARE & LUBRICANT PRODUCTS</h5>
						<div class="clear"></div>
						<p>Gapura Surya products comprise of various automotive / car care range such as car shampoo, car wax, various detailers and cleaning products and lubricant.</p>
						<div class="clear"></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="infos_bottom_footer2">
					<i class="fa fa-phone"></i> &nbsp;&nbsp;<?php echo $this->setting['contact_phone'] ?>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="visible-xs" style="height:4px;"><br></span>
					<i class="fa fa-envelope-o"></i> &nbsp;&nbsp;<a href="mailto:<?php echo $this->setting['email'] ?>"><?php echo $this->setting['email'] ?></a>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="visible-xs" style="height:4px;"><br></span>
					<i class="fa fa-whatsapp"></i> &nbsp;&nbsp;<a href="mailto:<?php echo $this->setting['contact_whatsapp'] ?>"><?php echo $this->setting['contact_whatsapp'] ?></a>
					</div>
					<div class="clear height-15"></div>
					<div class="t-copyrights">
						Copyright &copy; 2017. Gapura Surya. <span class="visible-xs" style="height:2px;"><br></span>Website design by <a href="http://www.markdesign.net/" target="_blank" title="Website Design Surabaya">Mark Design.</a>
						<div class="clear"></div>
					</div>
				</div>
				<div class="col-md-2">
					<div class="branchs_office fright text-right hide hidden">
						<span>Our Network Company</span>
						<div class="clear height-10"></div>
						<img src="<?php echo $this->assetBaseurl; ?>lgo_footer_branch.png" alt="" class="img-responsive">
						<div class="clear"></div>
					</div>

				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>
</footer>
*/ ?>