<section class="block_subpage_outer">

  <div class="default_sc blocks_top_about contact-1 back-white">
    <div class="prelatife container">
      <div class="insides content-text">
        <div class="row">
          <div class="col-md-6">
            <div class="contents">
              <h3 class="small-title"><?php echo $this->setting['contact_hero_title'] ?></h3>
              <h2><?php echo $this->setting['contact_hero_subtitle'] ?></h2>
              <?php echo $this->setting['contact_hero_content'] ?>
              <div class="clear height-20"></div>
              <dl class="dl-horizontal">
                <dt><i class="fa fa-phone"></i></dt>
                <dd><a href="tel:<?php echo $this->setting['contact_phone'] ?>"><?php echo $this->setting['contact_phone'] ?></a></dd>
                <div class="clear"></div>
                <dt><i class="fa fa-envelope-o"></i></dt>
                <dd><a href="mailto:<?php echo $this->setting['email'] ?>"><?php echo $this->setting['email'] ?></a></dd>
                <div class="clear"></div>
                <dt><i class="fa fa-whatsapp"></i></dt>
                <dd><a href="https://api.whatsapp.com/send?phone=62<?php echo $this->setting['contact_wa'] ?>">0<?php echo $this->setting['contact_wa'] ?></a></dd>
                <div class="clear"></div>
              </dl>

              <?php echo $this->setting['contact_hero_content_2'] ?>

            </div>
          </div>
          <div class="col-md-6">
            
          </div>
        </div>

        <div class="clear"></div>
      </div>
    </div>
  </div>

  <div class="default_sc blocks-middles-contact">
    <div class="prelatife container">
      <div class="insides content-text">
        <h3 class="small-title">Our Offices</h3>
        <div class="clear height-35"></div>
        <div class="blocksn_address">
          <div class="row default">
            <?php for ($i=1; $i < 3; $i++) { ?>
            <div class="col-md-6 col-sm-6 <?php echo ($i == 1)? 'borders' : ''; ?>">
              <div class="texts">
                <h2><?php echo $this->setting['contact_office_city_'.$i] ?></h2>

                <p><?php echo nl2br($this->setting['contact_office_address_'.$i]) ?></p>

                <p>Phone <?php echo $this->setting['contact_office_phone_'.$i] ?></p>

              </div>
            </div>
            <?php } ?>
          </div>
        </div>
        <div class="clear clearfix"></div>
      </div>
    </div>
  </div>

  <div class="default_sc blocks-middles-contact blocks_form back-white" id="contact-form">
    <div class="prelatife container">
      <div class="insides content-text" id="inquiry-form">
        <h3 class="small-title">Online Inquiry Form</h3>
        <div class="clear height-50"></div><div class="height-5"></div>

        <div class="blocksn-contact-form">
          <div class="box-form tl-contact-form text-left">
            <div class="inside">
              <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
                'type'=>'horizontal',
                'enableAjaxValidation'=>false,
                'clientOptions'=>array(
                    'validateOnSubmit'=>false,
                ),
                'htmlOptions' => array(
                    'enctype' => 'multipart/form-data',
                ),
            )); ?>
              <?php echo $form->errorSummary($model, '', '', array('class'=>'alert alert-danger', 'role'=>'alert')); ?>
              <?php if(Yii::app()->user->hasFlash('success')): ?>
                  <?php $this->widget('bootstrap.widgets.TbAlert', array(
                      'alerts'=>array('success'),
                  )); ?>
              <?php endif; ?>
              <div class="row default">
                <div class="col-md-12 col-sm-12">
                  <div class="form-group">
                      <label for="exampleInputName">NAME</label>
                      <div class="clear"></div>
                      <?php echo $form->textField($model, 'name', array('class'=>'form-control')); ?>
                  </div>
                </div>
                <div class="col-md-12 col-sm-12">
                  <div class="form-group">
                      <label for="exampleInputName">COMPANY</label>
                      <div class="clear"></div>
                      <?php echo $form->textField($model, 'company', array('class'=>'form-control')); ?>
                  </div>
                </div>
              </div>
              <div class="row default">
                <div class="col-md-12 col-sm-12">
                  <div class="form-group">
                      <label for="exampleInputName">EMAIL</label>
                      <div class="clear"></div>
                      <?php echo $form->textField($model, 'email', array('class'=>'form-control')); ?>
                  </div>
                </div>
                <div class="col-md-12 col-sm-12">
                  <div class="form-group">
                      <label for="exampleInputName">PHONE</label>
                      <div class="clear"></div>
                      <?php echo $form->textField($model, 'phone', array('class'=>'form-control')); ?>
                  </div>
                </div>
              </div>

              <div class="row default">
                <div class="col-md-12 col-sm-12">
                  <div class="form-group">
                      <label for="exampleInputMessage">MESSAGES</label>
                      <div class="clear"></div>
                      <div class="clear"></div>
                      <?php echo $form->textArea($model, 'body', array('class'=>'form-control', 'rows'=>3)); ?>

                  </div>
                </div>
              </div>
              <div class="clear height-10"></div>
              <div class="row default">
                <div class="col-md-12 col-sm-12">
                  <div class="form-group mb-0">
                    <div class="">
                      <div class="g-recaptcha" data-sitekey="6Lc6BFkUAAAAAI8fs0twzM9KcmrCdwIjjXX-1q9u"></div>
                    </div>
                    <div class="clear"></div>
                  </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="clear height-2"></div>
                    <button type="submit" class="btn btn-default btns-submit-bt"></button>
                </div>
              </div>
              <div class="clear"></div>
              <?php $this->endWidget(); ?>
              <div class="clear"></div>
            </div>
          </div>
        <!-- // end box form -->

        <script src='https://www.google.com/recaptcha/api.js'></script>
          <div class="clear"></div>
        </div>

        <div class="clear"></div>
      </div>
    </div>
  </div>


</section>