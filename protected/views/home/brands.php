<section class="block_subpage_outer">

  <div class="blocks_breadcrumb">
    <div class="prelatife container">
      <ol class="breadcrumb">
      <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
      <li class="active">Brands</li>
    </ol>
      <div class="clear"></div>
    </div>
  </div>

  <div class="subpage_default_outer">
    <div class="picture_illustrations p_brand hidden-xs" style="background-image: url('<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1586,375, '/images/static/'.$this->setting['brand_landing_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>')">
    </div>
    <?php if ($this->setting['brand_landing_image_res']): ?>
    <div class="picture_full visible-xs"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(774, 867, '/images/static/'.$this->setting['brand_landing_image_res'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></div>
    <?php endif ?>
    <div class="prelatife container">
        <div class="insides content-text text-center">
          <h1 class="title-page">BRANDS</h1>
          <div class="clear height-10"></div><div class="height-2"></div>

          <div class="lists_brands_data">
            <?php foreach ($brands as $key => $value): ?>
            <div class="items">
              <div class="row">
                <div class="col-md-4">
                  <div class="pic"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(393,311, '/images/brand/'.$value->image , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive"></div>
                </div>
                <div class="col-md-8">
                  <div class="infos">
                    <h3><?php echo $value->description->title ?></h3>
                    <?php echo $value->description->content ?>
                    <a href="<?php echo CHtml::normalizeUrl(array('/product/index', 'brand'=>$value->id)); ?>" class="btn btn-default btns_greens_def">View Products</a>
                    <div class="clear"></div>
                  </div>
                </div>
              </div>
            </div>
            <?php endforeach ?>
            <div class="clear"></div>
          </div>

          <div class="clear"></div>
        </div>      
    </div>
    <div class="clear"></div>
  </div>
  <!-- end subpage default -->

  <?php echo $this->renderPartial('//layouts/_block_bottom_form_info', array()); ?>
</section>