<section class="top_pages_product">
    <div class="prelatife container">
      <div class="inners">

        <div class="lefts_text">
          <h3 class="tops_sub">&nbsp;</h3>
          <div class="clear"></div>
          <h1>Gallery</h1>
          <div class="clear"></div>
        </div>

        <div class="row backgroundsn_rights">
          <div class="col-md-2">
            &nbsp;
          </div>
          <div class="col-md-10">
            <div class="pic_banner"><img src="<?php echo $this->assetBaseurl; ?>ill-heads-productsn.jpg" alt="" class="img-responsive"></div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
</section>

<section class="artikel-sec-1">
    <div class="prelatife container">
        <div class="row">

            <?php foreach ($model as $key => $value): ?>
            <div class="col-md-4">
                <div class="box-content">
                    <a data-fancybox="gallery" href="<?php echo Yii::app()->baseUrl.'/images/gallery/'. $value->image; ?>">
                    <img class="w-100" src="<?php echo Yii::app()->baseUrl.'/images/gallery/'. $value->image; ?>" alt="">
                    </a>
                    <div class="judul">
                        <p><?php echo $value->title ?></p>
                    </div>
                </div>
            </div>
            <?php endforeach ?>

        </div>
    </div>
</section>

<script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

<!-- <div class="padding-top-20 text-center box-pagination">
    <span class="inline-block">PAGE</span>&nbsp;
        <ul class="list-inline">
            <li class="active"><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
        </ul>
    <div class="clear"></div>
</div> -->
<div class="clear height-50"></div>
<div class="clear height-30"></div>