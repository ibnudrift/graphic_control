<section class="block_subpage_outer">

  <div class="default_sc blocks_top_about contact-1 back-white">
    <div class="prelatife container">
      <div class="insides content-text">
        <div class="row">
          <div class="col-md-6">
            <div class="contents">
              <h3 class="small-title"><?php echo $this->setting['career_hero_title'] ?></h3>
              <h2><?php echo $this->setting['career_hero_subtitle'] ?></h2>
              <?php echo $this->setting['career_hero_content'] ?>
              <p class="mail"><a href="mailto:<?php echo $this->setting['career_email'] ?>"><?php echo $this->setting['career_email'] ?></a></p>
              <div class="clear height-10"></div>
              <p><?php echo $this->setting['career_hero_content2'] ?></p>

              <div class="clear"></div>
            </div>
          </div>
          <div class="col-md-6">
            
          </div>
        </div>

        <div class="clear"></div>
      </div>
    </div>
  </div>

  <div class="default_sc blocks-middles-contact">
    <div class="prelatife container">
      <div class="insides text-center content-text">
          <?php 
          $model_career = Career::model()->findAll();
          ?>
          <?php if (count($model_career) > 0): ?>
          <?php echo Tt::t('front', 'POSISI TERSEDIA'); ?>
          <?php else: ?>
          <h3 class="subs_title wow fadeInDown"><?php echo Tt::t('front', 'Maaf, saat ini belum terdapat lowongan posisi yang terdaftar.'); ?></h3>
          <?php endif ?>

          <div class="clear height-40"></div>

          <div class="list-widget-careers block-widget text-left">
            <?php foreach ($model_career as $key => $value){ ?>
            <div class="items wow fadeInDown">
              <div class="tops">
                <h3><?php echo $value->title ?></h3>
              </div>
              <div class="row default">
                <div class="col-md-4 col-sm-4">
                  <h5>deskripsi pekerjaan</h5>
                  <?php echo $value->deskripsi ?>
                </div>
                <div class="col-md-4 col-sm-4">
                  <h5>kualifikasi</h5>
                  <?php echo $value->kualifikasi ?>
                </div>
                <div class="col-md-4 col-sm-4">
                  <h5>lokasi bekerja</h5>
                  <p><?php echo $value->lokasi_kerja ?></p>
                  <div class="clear height-50"></div>
                  <!-- <a href="#" class="btn btn-sents-cv">KIRIM LAMARAN</a> -->
                </div>
              </div>
            </div>
            <!-- end items -->
            <?php } ?>
          </div>

          <div class="clear"></div>
        </div>
    </div>
  </div>

</section>