<section class="top_pages_product">
    <div class="prelatife container">
      <div class="inners">

        <div class="lefts_text">
          <h3 class="tops_sub">&nbsp;</h3>
          <div class="clear"></div>
          <h1>Article</h1>
          <div class="clear"></div>
        </div>

        <div class="row backgroundsn_rights">
          <div class="col-md-2">
            &nbsp;
          </div>
          <div class="col-md-10">
            <div class="pic_banner"><img src="<?php echo $this->assetBaseurl; ?>ill-heads-productsn.jpg" alt="" class="img-responsive"></div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
</section>

<section class="art-det-sec-1">
    <div class="prelatife container3">
        <div class="row">
            <div class="col-md-8">
                <div class="title">
                    <p><?php echo $detail->description->title ?></p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="tanggal">
                    <p><?php echo date('d F Y', strtotime($detail->date_input)) ?></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-60">
                <div class="image">
                    <img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(980,450, '/images/blog/'.$detail->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block">
                </div>
            </div>
            <div class="col-md-60">
                <div class="content">
                    <?php echo $detail->description->content ?>
                </div>
            </div>
        </div>
        <div class="hr-art"></div>
    </div>
</section>

<?php
$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('active = "1"');
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;

$criteria->addCondition('t.id != :ids');
$criteria->params[':ids'] = $_GET['id'];

$criteria->order = 'date_input DESC';
$criteria->limit = 3;

$dataBlog = Blog::model()->findAll($criteria);
?>
<?php if (count($dataBlog) > 0): ?>
<section class="art-det-sec-2">
    <div class="prelatife container">
        <div class="row">
            <div class="col-md-60">
                <div class="title">
                    <p>Article Lainnya</p>
                </div>
            </div>
                <?php foreach ($dataBlog as $key => $value){ ?>
                <div class="col-md-4">
                    <div class="box-content">
                        <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=>$value->id)); ?>"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(313,204, '/images/blog/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive w-100"></a>
                        <div class="judul">
                            <p><?php echo $value->description->title ?></p>
                        </div>
                    </div>
                </div>
                <?php } ?>
        </div>
    </div>
</section>
<?php endif ?>