<section class="block_subpage_outer">

  <div class="default_sc blocks_top_about about-1 back-white" style="background-image: url('<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1920,800, '/images/static/'.$this->setting['about_hero_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>')">
    <!-- <div class="banners-fulls visible-xs"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1920,800, '/images/static/'.$this->setting['about_hero_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block"></div> -->
    <div class="prelatife container">
      <div class="insides content-text">
        <div class="row">
          <div class="col-md-6">
            <div class="contents">
              <h3 class="small-title"><?php echo $this->setting['about_hero_title'] ?></h3>
              <h2><?php echo $this->setting['about_hero_subtitle'] ?></h2>
              <?php echo $this->setting['about_hero_content'] ?>
            </div>
          </div>
          <div class="col-md-6">
            
          </div>
        </div>

        <div class="clear"></div>
      </div>
    </div>
  </div>

  <div class="default_sc blocks_top_about about-2 back-white" style="background-image: url('<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1920,800, '/images/static/'.$this->setting['about_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>')">
    <div class="prelatife container">
      <div class="insides content-text">
        <div class="row">
          <div class="col-md-6">
          </div>
          <div class="col-md-6">
            <div class="contents">
              <h3 class="small-title"><?php echo $this->setting['about_title'] ?></h3>
              <h2><?php echo $this->setting['about_subtitle'] ?></h2>
              <?php echo $this->setting['about_content'] ?>
            </div>
          </div>
        </div>

        <div class="clear"></div>
      </div>
    </div>
  </div>
  <?php 
  // background-image: url('<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1920,800, '/images/static/'.$this->setting['about_team_image'] , array('method' => 'adaptiveResize', 'quality' => '90'))')
   ?>
  <div class="default_sc blocks_top_about about-3 back-white">
    <div class="prelatife container">
      <div class="insides content-text">
        <div class="row">
          <div class="col-md-2">
          </div>
          <div class="col-md-8 text-center">
            <div class="contents">
              <h3 class="small-title"><?php echo $this->setting['about_team_title'] ?></h3>
              <h2><?php echo $this->setting['about_team_subtitle'] ?></h2>
              <?php echo $this->setting['about_team_content'] ?>
            </div>
          </div>
          <div class="col-md-2">
            
          </div>
        </div>

        <div class="clear"></div>
      </div>
    </div>
  </div>

  <div class="default_sc blocks_top_about about-4 blocks-visionMission">
    <div class="prelatife container">
      <div class="insides content-text">
        <div class="row">
          <div class="col-md-6 col-sm-6 borders">
            <div class="contents">
              <h3 class="small-title">Visi</h3>
              <?php echo $this->setting['about_visi'] ?>
              <div class="pictsn"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(300,300, '/images/static/'.$this->setting['about_visi_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block"></div>
            </div>
          </div>
          <div class="col-md-6 col-sm-6">
            <div class="contents">
              <h3 class="small-title">Misi</h3>
              <?php echo $this->setting['about_misi'] ?>
              <div class="pictsn"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(300,300, '/images/static/'.$this->setting['about_misi_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block"></div>
            </div>
          </div>
        </div>

        <div class="clear"></div>
      </div>
    </div>
  </div>



  <?php // echo $this->renderPartial('//layouts/_block_bottom_form_info', array()); ?>
</section>

<script type="text/javascript">
  // set height image right 2
  $(window).load(function(){
    var heightf = $('.pictures_ins_about_bottom img.pleft_h').height() / 2;
    $('.pictures_ins_about_bottom .picts_right img').css('height', heightf+'px');
  })
</script>