<section class="top_pages_product">
    <div class="prelatife container">
      <div class="inners">

        <div class="lefts_text">
          <h3 class="tops_sub">&nbsp;</h3>
          <div class="clear"></div>
          <h1>Article</h1>
          <div class="clear"></div>
        </div>

        <div class="row backgroundsn_rights">
          <div class="col-md-2">
            &nbsp;
          </div>
          <div class="col-md-10">
            <div class="pic_banner"><img src="<?php echo $this->assetBaseurl; ?>ill-heads-productsn.jpg" alt="" class="img-responsive"></div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
</section>

<section class="artikel-sec-1">
    <div class="prelatife container">
        <div class="row">

            <?php foreach ($dataBlog->getData() as $key => $value): ?>
            <div class="col-md-4">
                <div class="box-content">
                    <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=>$value->id)); ?>">
                    <img class="w-100" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(313,204, '/images/blog/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt=""></a>
                    <div class="judul">
                        <a href="<?php echo CHtml::normalizeUrl(array('/blog/detail', 'id'=>$value->id)); ?>">
                        <p><?php echo $value->description->title ?></p>
                        </a>
                    </div>
                </div>
            </div>
            <?php endforeach ?>

        </div>
    </div>
</section>

<div class="padding-top-20 text-center box-pagination">
    <!-- <span class="inline-block">PAGE</span>&nbsp; -->
    <?php $this->widget('CLinkPager', array(
          'pages' => $dataBlog->getPagination(),
          'header' => '',
      )) ?>
    <div class="clear"></div>
</div>
<div class="clear height-50"></div>
<div class="clear height-30"></div>