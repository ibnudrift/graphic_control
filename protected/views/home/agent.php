<section class="block_subpage_outer">
  <div class="blocks_breadcrumb">
    <div class="prelatife container">
      <ol class="breadcrumb">
      <li><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
      <li class="active">Become An Agent</li>
    </ol>
      <div class="clear"></div>
    </div>
  </div>

  <div class="blocks_top_about back-white">
    <div class="prelatife container">
      <div class="insides content-text">
        <div class="row">
          <div class="col-md-6">
            <div class="picture"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(610,670, '/images/static/'.$this->setting['agent_image'] , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="" class="img-responsive"></div>
          </div>
          <div class="col-md-6">
            <div class="info descriptions">
              <h1 class="title-page"><?php echo $this->setting['agent_title'] ?></h1>
              <h3><?php echo $this->setting['agent_subtitle'] ?></h3>
              <?php echo $this->setting['agent_content'] ?>
              <div class="clear"></div>
            </div>
          </div>
        </div>
        <div class="clear"></div>
      </div>
    </div>
  </div>

  <div class="blocks_middle_greenAgent">
    <div class="prelatife container">
      <div class="insides maw670 tengah">
        <?php echo $this->setting['agent_sec2_content'] ?>
        <div class="clear"></div>
      </div>
    </div>
  </div>

  <section class="blocks_agent_greys_middle">
    <div class="prelatife container">
      <div class="insides tengah maw670 text-center">
        <?php echo $this->setting['agent_sec3_content'] ?>
        <div class="clear height-30"></div>
        <h4>Contact Us Now!</h4>
        <div class="clear height-30"></div>
        <div class="info">
          <i class="fa fa-phone"></i> &nbsp;(+62) 31 7505 999
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br class="visible-xs">
          <i class="fa fa-envelope-o"></i> &nbsp;<a href="mailto:admin@gapurasurya.com">admin@gapurasurya.com</a>
          <div class="clear"></div>
        </div>
      </div>
    </div>
  </section>

  <?php echo $this->renderPartial('//layouts/_block_bottom_form_info', array()); ?>
</section>