<!-- slider -->
<section class="slider">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <?php
                    $criteria = new CDbCriteria;
                    $criteria->with = array('description');
                    $criteria->addCondition('active = 1');
                    $criteria->addCondition('description.language_id = :language_id');
                    $criteria->params[':language_id'] = $this->languageID;
                    $criteria->order = 'sort ASC';
                    $dataSlide = Slide::model()->findAll($criteria);
                    ?>
                <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <?php foreach ($dataSlide as $key => $value): ?>
                            <?php if ($value->type == 1): ?>
                                <div class="item <?php if ($key == 0): ?>active<?php endif ?>">
                                    <?php if ($value->description->url != ''): ?>
                                    <?php // echo $value->description->url ?>
                                    <!-- <a class="fancy_video" href="https://www.youtube.com/watch?v=diIMCuQwxLw"> -->
                                    <a class="fancy_video" href="https://www.youtube.com/embed/<?php echo Common::getVYoutube($value->description->url) ?>?autoplay=1&rel=0">
                                    <?php endif ?>
                                    <img class="alat hidden-xs" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1552,553, '/images/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="alat">
                                    <img class="alat visible-xs" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(774,867, '/images/'.$value->image2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="alat">
                                    <?php if ($value->description->url != ''): ?>
                                    </a>
                                    <?php endif ?>
                                </div>
                            <?php else: ?>
                                <div class="item <?php if ($key == 0): ?>active<?php endif ?>">
                                    <img class="alat hidden-xs" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(1552,553, '/images/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="alat">
                                    <img class="alat visible-xs" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(774,867, '/images/'.$value->image2 , array('method' => 'adaptiveResize', 'quality' => '90')) ?>" alt="alat">
                                    <?php if ($value->hide_teks != 1): ?>
                                    <div class="carousel-capt">
                                        <h2 class="title"><?php echo nl2br($value->description->title) ?></h2>
                                        <p class="subtitle"><?php echo $value->description->content ?>
                                        </p>
                                        <?php if ($value->description->url != ''): ?>
                                        <?php
                                            if (strpos($value->description->url, 'youtu') > 0) {
                                                ?>
                                                <a  class="btn fancy_video" href="https://www.youtube.com/embed/<?php echo Common::getVYoutube($value->description->url) ?>?autoplay=1&rel=0"><?php echo $value->description->url_teks ?></a>
                                                <?php
                                            } else {
                                                ?>
                                                <a  class="btn" href="<?php echo $value->description->url ?>"><?php echo $value->description->url_teks ?></a>
                                                <?php
                                            }
                                        ?>
                                        <?php endif ?>
                                    </div>
                                    <?php endif ?>
                                </div>
                            <?php endif ?>
                        <?php endforeach ?>

                    </div>

                    <!-- membuat panah next dan previous -->
                    <a class="left-carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="fa fa-angle-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right-carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="fa fa-angle-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- akhir slider -->
<style type="text/css">
    .slider .right-carousel-control{
        right: -25px;
    }
    .slider .left-carousel-control{
        left: -25px;
    }
</style>

<!-- portfolio -->
<section class="portfolio">
    <div class="container">
        <div class="tz-gallery">
  
        <div class="row">
            <div class="col-sm-4">
                <div class="items">
                    <div class="thumbnail">
                        <a class="lightbox" href="<?php echo $this->assetBaseurl; ?>gedung.png">
                        <img src="<?php echo $this->assetBaseurl; ?>how-to-built.png" alt="gedung">
                        </a>
                    </div>
                    <div class="capt">
                        <h3>HOW GRAPHIC <br> CONTROL BUILT <br> REPUTATION IN <br> INDONESIA</h3>
                        <img src="<?php echo $this->assetBaseurl; ?>icon.png"><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>"><p>Learn more about us</p></a>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="items">
                    <div class="thumbnail">
                        <a class="lightbox" href="<?php echo $this->assetBaseurl; ?>alat2.png">
                        <img src="<?php echo $this->assetBaseurl; ?>alat2.png" alt="alat">
                        </a>
                    </div>
                    <div class="capt">
                        <h3>THE WORLD’S <br> LEADING <br> PRODUCTS IS AT <br> OUR HANDS</h3>
                        <img src="<?php echo $this->assetBaseurl; ?>icon.png"><a href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>"><p>Learn more about us</p></a>
                    </div>p
                </div>
            </div>

            <div class="col-sm-4">
                <div class="items">
                    <div class="thumbnail">
                        <a class="lightbox" href="<?php echo $this->assetBaseurl; ?>women.png">
                        <img src="<?php echo $this->assetBaseurl; ?>women.png" alt="women">
                        </a>
                    </div>
                    <div class="capt">
                        <h3>THE HELP YOU <br> NEED IS JUST A <br> CALL AWAY</h3><br>
                        <img src="<?php echo $this->assetBaseurl; ?>icon.png"><a href="<?php echo CHtml::normalizeUrl(array('/home/contact')); ?>"><p>Learn more about us</p></a>
                    </div>
                </div>
            </div>  
        </div>
    </div>
</section>
<!-- akhir portfolio -->

<!-- product -->
<section class="product">
    <div class="container">
        <div class="tops text-center">
            <h3 class="sub_title">Our Featured Products</h3>
        </div>

        <div class="list_featured_products">
            <div class="row">
                <?php foreach ($product as $key => $value): ?>
                <div class="col-md-4 col-sm-4">
                    <div class="items">
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/productDetail', 'id'=>$value->id)); ?>">
                            <img class="img-responsive center-block" src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(171,171, '/images/product/'.$value->image , array('method' => 'adaptiveResize', 'quality' => '90')) ?>">
                        </a>
                        <div class="clear"></div>
                        <div class="capt">
                            <div class="title text-center"><?php echo $value->description->name ?></div>
                            <div class="subtitle text-center cat2"><?php echo $value->category->description->name; ?><?php // echo $value->description->subtitle ?></div>
                        </div>
                        <div class="text-center">
                        <a href="<?php echo CHtml::normalizeUrl(array('/home/productDetail', 'id'=>$value->id)); ?>" class="btn">Lebih Lanjut</a>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>

            <div class="padding-top-50 text-center">
                <img class="plus" src="<?php echo $this->assetBaseurl; ?>icon-plus.png"><a class="produk" href="<?php echo CHtml::normalizeUrl(array('/home/products')); ?>">Lihat Produk Lainnya</a>
            </div>
            <div class="height-40"></div>
        </div>

        <div class="clearfix"></div>
    </div>
</section>
<!-- akhir product -->

<!-- browse -->
<section class="browse hide hidden">
    <div class="container">
        <h2 class="text-center">Browse Our Complete Products By Category</h2>
        <div class="clear height-35"></div>
        <div class="row">
              <?php
                  $criteria = new CDbCriteria;
                  $criteria->with = array('description');
                  $criteria->addCondition('description.language_id = :language_id');
                  $criteria->params[':language_id'] = $this->languageID;
                  $criteria->addCondition('t.parent_id = :parent_id');
                  $criteria->params[':parent_id'] = 0;
                  $criteria->addCondition('t.type = :type');
                  $criteria->params[':type'] = 'category';
                  $criteria->order = 'sort ASC';
                  $criteria->limit = 5;
                  $categories = PrdCategory::model()->findAll($criteria);

              ?>
              <?php foreach ($categories as $key => $value): ?>
            <div class="col-md-15">
                <div class="title"><a href="<?php echo CHtml::normalizeUrl(array('/home/products', 'category'=>$value->id)); ?>"><?php echo $value->description->name ?></a></div>
                <div class="subtitle-short">
                  <?php
                      $criteria = new CDbCriteria;
                      $criteria->with = array('description');
                      $criteria->addCondition('description.language_id = :language_id');
                      $criteria->params[':language_id'] = $this->languageID;
                      $criteria->addCondition('t.type = :type');
                      $criteria->params[':type'] = 'category';
                      $criteria->addCondition('t.parent_id = :parent_id');
                      $criteria->params[':parent_id'] = $value->id;
                      $criteria->order = 'sort ASC';
                      // $criteria->limit = 5;
                      $categories2 = PrdCategory::model()->findAll($criteria);

                  ?>
                    <ul>
                        <?php foreach ($categories2 as $k => $v): ?>
                        <li><a href="<?php echo CHtml::normalizeUrl(array('/home/products', 'category'=>$value->id, 'subcategory'=>$v->id)); ?>"><?php echo $v->description->name ?></a></li>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
              <?php endforeach ?>
              <?php /*
            <div class="col-md-15 ">
                <div class="title">ELECTRICAL TEST</div>
                <div class="subtitle-short">
                    <ul>
                        <li>32-Bit MCU </li>
                        <li>8-Bit MCU D</li>
                        <li>All MCU </li>
                        <li>MCU Software</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-15 ">
                <div class="title">MATERIAL TEST</div>
                <div class="subtitle-short">
                    <ul>
                        <li>Bluetooth Development </li>
                        <li>zigbee Tools</li>
                        <li>Thread Tools</li>
                        <li>Proprietary Tools</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-15 ">
                <div class="title">Environment & <br> Water Test</div>
                <div class="subtitle-short">
                    <ul>
                        <li>Audio Tools</li>
                        <li>Clock Tools</li>
                        <li>USBXpress&reg; Tools</li>
                        <li>Software</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-15 ">
                <div class="title">THERMAL IMAGING</div>
                <div class="subtitle-short">
                    <ul>
                        <li>Bluetooth Development</li>
                        <li>Zigbee Tools</li>
                        <li>Thread Tools</li>
                        <li>Proprietary Tools</li>
                    </ul>
                </div>
            </div>
            */ ?>
        </div>
    </div>
</section>
<!-- akhir browse -->

<!-- explore -->
<section class="explore">
    <div class="container">
        
        <div class="tops_title padding-bottom-35">
            <h2 class="text-center">Explore Our Products by Industry Solutions</h2>
        </div>

        <div class="lists_explores_industry lists-default-solutions-datas home_list">
            <div class="row text-center owl-carousel owl-theme">
                    <?php foreach ($dataBrand as $key => $value): ?>
                    <div class="col-md-12">
                        <div class="items">
                            <div class="picture"><img src="<?php echo Yii::app()->baseUrl.ImageHelper::thumb(365,300, '/images/brand/'.$value->image , array('method' => 'resize', 'quality' => '90')) ?>" alt="" class="img-responsive center-block"></div>
                            <div class="info">
                                <h5 class="title"><?php echo $value->description->title ?></h5>
                                <p><?php echo $value->description->content ?></p>
                                <a href="<?php echo CHtml::normalizeUrl(array('/home/products', 'solution'=>$value->id)); ?>" class="btn btn-default btns-def-yellow">Lebih Lanjut</a>
                                <div class="clear clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>
 
            </div>
            <div class="clear"></div>
        </div>

        <div class="padding-top-50 text-center">
            <img class="plus" src="<?php echo $this->assetBaseurl; ?>icon-plus.png"><a class="produk" href="<?php echo CHtml::normalizeUrl(array('/home/solution')); ?>">Lihat Industry Solutions Lainya</a>
        </div>
        </div>
        <div class="height-40"></div>
    </div>
</section>
<!-- akhir explore -->


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.3/assets/owl.carousel.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.3/assets/owl.theme.default.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.3/owl.carousel.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $('.owl-carousel').owlCarousel({
            loop:true,
            margin:10,
            nav: true,
            dots: false,
            autoplay: true,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    nav:true
                },
                600:{
                    items:2,
                    nav:false
                },
                1000:{
                    items:3,
                    nav:true,
                    loop:false
                }
            }
        });

    });
</script>